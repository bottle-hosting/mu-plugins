<?php
// Exit if accessed directly.
if (!defined('ABSPATH')) exit;
include_once(ABSPATH . 'wp-admin/includes/plugin.php');


$BOTTLE_META_INFO = defined('BOTTLE_META_INFO') ? constant('BOTTLE_META_INFO') : 1;
$BOTTLE_SVG = defined('BOTTLE_SVG') ? constant('BOTTLE_SVG') : 1;
$BOTTLE_DISABLE_COMMENTS = defined('BOTTLE_DISABLE_COMMENTS') ? constant('BOTTLE_DISABLE_COMMENTS') : 1;
$BOTTLE_IMAGE_OPTIMIZER = defined('BOTTLE_IMAGE_OPTIMIZER') ? constant('BOTTLE_IMAGE_OPTIMIZER') : 1;
$BOTTLE_CDN = defined('BOTTLE_CDN') ? constant('BOTTLE_CDN') : 1;
$BOTTLE_CACHE = defined('BOTTLE_CACHE') ? constant('BOTTLE_CACHE') : 1;
$BOTTLE_MAILER = defined('BOTTLE_MAILER') ? constant('BOTTLE_MAILER') : 1;

/**
 * Meta Info Remover
 */
if ($BOTTLE_META_INFO && !is_plugin_active('meta-generator-and-version-info-remover/meta_generator_and_version_info_remover.php')) {
  require WPMU_PLUGIN_DIR . '/bottle/meta-generator-and-version-info-remover/meta_generator_and_version_info_remover.php';
}
/**
 * Safe SVG
 */
if ($BOTTLE_SVG  && !is_plugin_active('safe-svg/safe-svg.php')) {
  require WPMU_PLUGIN_DIR . '/bottle/safe-svg/safe-svg.php';
}

/**
 * Disable Comments
 */
if ($BOTTLE_DISABLE_COMMENTS) {
  require WPMU_PLUGIN_DIR . '/bottle/disable-comments/disable-comments.php';
}

/**
 * Image Optimizer
 */
if ($BOTTLE_IMAGE_OPTIMIZER  && !is_plugin_active('png-to-jpg/png-to-jpg.php')) {
  require WPMU_PLUGIN_DIR . '/bottle/png-to-jpg/png-to-jpg.php';
  if (!defined('BOTTLE_IMAGE_OPTIMIZER_DEFAULTS')) {
    $defaults = array(
      'general' => array(
        'upload_convert' => 2,
        'jpg_quality' => '85',
        'only_lower' => 'checked',
        'leave_original' => 'unchecked',
        'autodetect' => 'unchecked'
      )
    );
    update_option('png_to_jpg_settings', $defaults);
    define('BOTTLE_IMAGE_OPTIMIZER_DEFAULTS', 1);
  }
}


/**
 * WP Mailer
 */
if ($BOTTLE_MAILER) {
  if (!is_plugin_active('wp-mail-smtp/wp_mail_smtp.php') || !is_plugin_active('wp-mail-smtp-pro/wp_mail_smtp.php')) {
    define('WPMS_MAILGUN_DOMAIN', 'mg.bottle.sh');
    define('WPMS_MAILGUN_REGION', 'US');
    require WPMU_PLUGIN_DIR . '/bottle/wp-mail-smtp/wp_mail_smtp.php';
  }
}

/**
 * CDN Enabler
 */
if ($BOTTLE_CDN && !is_plugin_active('cdn-enabler/cdn-enabler.php')) {
  require WPMU_PLUGIN_DIR . '/bottle/cdn-enabler/cdn-enabler.php';
}

/**
 * NGINX Helper
 */
if ($BOTTLE_CACHE && !is_plugin_active('nginx-helper/nginx-helper.php')) {
  require WPMU_PLUGIN_DIR . '/bottle/nginx-helper/nginx-helper.php';
}
