#!/bin/bash
ARCHIVE='mu-plugins'
DIR="./mu-plugins"
rm -rf $DIR
wget -O $ARCHIVE.zip https://gitlab.com/bottle-hosting/mu-plugins/-/archive/master/mu-plugins-master.zip
unzip -qq -o mu-plugins.zip && mv "$ARCHIVE-master/$ARCHIVE" $ARCHIVE
rm -rf "$ARCHIVE-master"
rm $ARCHIVE.zip

find ${DIR} -type d -not -perm 755 -exec chmod 755 {} \;
find ${DIR} -type f -not -perm 644 -exec chmod 644 {} \;
