<?php


class BottleMuUpdate
{
  private $plugins = array(
    'cdn-enabler' => 'https://downloads.wordpress.org/plugin/cdn-enabler.zip',
    'meta-generator-and-version-info-remover' => 'https://downloads.wordpress.org/plugin/meta-generator-and-version-info-remover.zip',
    'nginx-helper' => 'https://downloads.wordpress.org/plugin/nginx-helper.zip',
    'png-to-jpg' => 'https://downloads.wordpress.org/plugin/png-to-jpg.zip',
    'wp-mail-smtp' => 'https://downloads.wordpress.org/plugin/wp-mail-smtp.zip',
  );

  public function download()
  {
    foreach ($this->plugins as $name => $url) {
      $this->update_mu_plugin($name, $url);
    }
  }

  /**
   * Download and extract plugin
   */
  private function update_mu_plugin($name, $url)
  {
    $zip = new ZipArchive;
    $filename = $name . '.zip';
    $destination_path = dirname(__FILE__) . '/' . $filename;
    file_put_contents($destination_path, fopen($url, 'r'));
    $res = $zip->open($filename);
    if ($res === TRUE) {
      $path = dirname(__FILE__) . "/mu-plugins/bottle/";
      $zip->extractTo($path);
      $zip->close();
      echo $name  . ' updated...' . PHP_EOL;
    } else {
      echo $name . ' failed...' . PHP_EOL;
    }
  }


  /**
   * Run cleanup operation
   */
  public function clean_up()
  {
    $current = dirname(__FILE__);
    foreach ($this->plugins as $name => $url) {
      unlink($current . '/' . $name . '.zip');
    }
  }
}


$update = new BottleMuUpdate();
$update->download();
$update->clean_up();

exit("completed...");
